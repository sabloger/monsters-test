package utils

import (
	"bufio"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

// Args keeps the program's arguments
type Args struct {
	FileName string
	N        int
}

var wg = sync.WaitGroup{}

func init() {
	// Seeding the random package to have a better randomness!
	rand.Seed(time.Now().UnixNano())
}

const commandHelp = "Usage:\n\tmonsters-test number-of-monsters world-map-file\nFor example:\n\tmonsters-test 12 world_map_small.txt"

// ParseArgs is a very simpler Arguments parser for monsters test:
// this get two args, an integer value as N (monters no.) and a string as WorldMap file name.
func ParseArgs() (args Args) {
	if len(os.Args) != 3 {
		log.Fatal("Invalid number of arguments! \n", commandHelp)
	}

	for i, arg := range os.Args {
		if i == 0 {
			continue
		}
		if n, err := strconv.Atoi(arg); err == nil {
			args.N = n
		} else {
			args.FileName = arg
		}
	}
	if args.N == 0 || args.FileName == "" {
		log.Fatal("Invalid arguments! \n", commandHelp)
	}
	return
}

// ParseWorldMapFile will parse worldMap file and build it in a graph format
func ParseWorldMapFile(fileName string) {
	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal("Error in opening file:", err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		wg.Add(1)
		go parseCityLine(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal("Error in reading file:", err)
	}
	wg.Wait()
}

func parseCityLine(line string) {
	a := strings.Split(line, " ")

	// Check if line contents are in a correct format
	if len(a) < 2 || len(a) > 5 {
		log.Fatal("Invalid City line:", line)
	}
	var city *City

	for i, value := range a {
		if i == 0 {
			city = WorldMap.AddOrGetCity(a[0])
		} else {
			link := strings.Split(value, "=")

			// Check if direction is correctly separated with equal sign "="
			if len(link) != 2 {
				log.Fatal("Invalid direction info: '", value, "' in line:", line)
			}

			neighborCity := WorldMap.AddOrGetCity(link[1])
			WorldMap.AddRoad(city, neighborCity, link[0])
		}
	}
	wg.Done()
}
