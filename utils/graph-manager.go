package utils

import (
	"fmt"
	"log"
	"math/rand"
	"sort"
	"sync"
)

// City represents a city in the world
type City struct {
	name                   string
	monsters               []int
	lock                   sync.Mutex
	isDestroyed, isInFight bool
}

// Road keeps the destination and the direction
type Road struct {
	destination *City
	direction   int
}

// World is the graph of the cities
type World struct {
	cities []*City
	roads  map[string][]Road
	lock   sync.Mutex
}

// Keeping the directions as enums for better performance and lower memory usage
const (
	DirectionNorth = iota
	DirectionEast
	DirectionSouth
	DirectionWest
)

var (
	directionsMap = map[string]int{
		"north": DirectionNorth,
		"east":  DirectionEast,
		"south": DirectionSouth,
		"west":  DirectionWest,
	}
	directionsReverseMap = map[int]string{
		DirectionNorth: "north",
		DirectionEast:  "east",
		DirectionSouth: "south",
		DirectionWest:  "west",
	}
	// WorldMap is the only instance of the world!
	WorldMap World
)

// AddMonster will add a monster to a city's monsters list to check for fight or city's availability
func (city *City) AddMonster(id int) bool {
	city.lock.Lock()
	defer city.lock.Unlock()
	city.monsters = append(city.monsters, id)
	if len(city.monsters) >= 2 {
		city.isInFight = true
	}
	if len(city.monsters) > 2 {
		return false
	}
	return true
}

// RemMonster will remove a monster from a city if the city isn't in fight!
func (city *City) RemMonster(id int) bool {
	city.lock.Lock()
	defer city.lock.Unlock()

	// Is the city already in a fight?
	if city.isInFight {
		return false
	}
	sort.Ints(city.monsters)
	index := sort.SearchInts(city.monsters, id)
	if index < len(city.monsters) {
		city.monsters = append(city.monsters[0:index], city.monsters[index+1:]...)
	}
	return true
}

// IsCityAvailable check the availability of the city with checking number of the current monsters in it or in-progress fight or destroyed before.
func (city *City) IsCityAvailable() bool {
	city.lock.Lock()
	defer city.lock.Unlock()
	return len(city.monsters) <= 1 && !city.isInFight && !city.isDestroyed
}

// CheckFight will checks a city if a fight is.
// Returns another monster's id if is in fight
// Returns -1 if the city is in peace
// Returns -2 if the city is destroyed before
// (never) Returns -3 only to prevent syntax error
func (city *City) CheckFight(checkerMonsterID int) int {
	city.lock.Lock()
	defer city.lock.Unlock()
	if city.isDestroyed {
		// This city has been destroyed before unlocking the sync
		return -2
	}
	if len(city.monsters) > 1 {
		for _, monsterID := range city.monsters {
			if monsterID != checkerMonsterID {
				// There a Fight!!
				city.isInFight = true
				city.isDestroyed = true
				return monsterID
			}
		}
		log.Fatal("Bad monstersList exception:", city.name, city.monsters)
		return -3
	}
	// Fight not found!
	return -1

}

// String returns the city name
func (city *City) String() string {
	return city.name
}

// SetDirection sets the direction of a road. Mapping string name to enums
func (road *Road) SetDirection(direction string) {
	road.direction = directionsMap[direction]
}

// GetDirection gets the direction of a road. Mapping enums to string name
func (road *Road) GetDirection() string {
	return directionsReverseMap[road.direction]
}

// AddOrGetCity will create the city if is not exists before, will returns the city anyway.
func (world *World) AddOrGetCity(cityName string) *City {
	world.lock.Lock()
	city := world.GetCity(cityName)
	if city == nil {
		city = &City{name: cityName}
		world.cities = append(world.cities, city)
	}
	defer world.lock.Unlock()
	return city
}

// AddRoad adds a road to an existing city
func (world *World) AddRoad(cityFrom, cityTo *City, direction string) {
	world.lock.Lock()
	if world.roads == nil {
		world.roads = make(map[string][]Road)
	}
	road := Road{
		destination: cityTo,
	}
	road.SetDirection(direction)
	world.roads[cityFrom.name] = append(world.roads[cityFrom.name], road)
	world.lock.Unlock()
}

// Print will prints the world map in the same format as the input file.
func (world *World) Print() {
	world.lock.Lock()
	for i := 0; i < len(world.cities); i++ {
		str := world.cities[i].String() + " "
		for r := 0; r < len(world.roads[world.cities[i].name]); r++ {
			str += fmt.Sprintf("%s=%s ", world.roads[world.cities[i].name][r].GetDirection(), world.roads[world.cities[i].name][r].destination)
		}
		fmt.Println(str)
	}
	world.lock.Unlock()
}

// GetCity will get city by name if exists in the world
func (world *World) GetCity(cityName string) *City {
	for _, city := range world.cities {
		if city.name == cityName {
			return city
		}
	}
	return nil
}

// GetRandomCity will find a city randomly add the monster to it the returns the city
// Panics if there is not any free/available city
func (world *World) GetRandomCity(monsterID int) *City {
	world.lock.Lock()
	defer world.lock.Unlock()

	cnt := len(world.cities)
	checkedCities := make(map[string]bool)
	var haveFound bool
	for len(checkedCities) < cnt && !haveFound {
		rnd := rand.Int() % cnt
		checkedCities[world.cities[rnd].name] = true
		if world.cities[rnd].IsCityAvailable() {

			// Adding a monster to returning city:
			world.cities[rnd].AddMonster(monsterID)

			return world.cities[rnd]
		}
	}
	log.Fatal("No available City for new monsters found!")
	return nil
}

// GetRandomRoad returns a random road of the input city for a monster. (adds the monster to the city before return)
// Returns a (fake) LoopBack road to the input city if there is not any road or roads to any available city.
func (world *World) GetRandomRoad(city *City, monsterID int) Road {
	world.lock.Lock()
	defer world.lock.Unlock()

	roads := world.roads[city.name]
	cnt := len(roads)
	if cnt == 0 {
		// Returning a loopback road because there are not any road!
		return Road{
			destination: city,
		}
	}
	var checkedRoads = make(map[Road]bool)
	var haveFound bool
	for len(checkedRoads) < cnt && !haveFound {
		rnd := rand.Int() % cnt
		checkedRoads[roads[rnd]] = true
		if roads[rnd].destination.IsCityAvailable() {

			// Adding a monster to returning city:
			if !roads[rnd].destination.AddMonster(monsterID) {
				panic("Bad exception on Adding monster to city, GetRandomRoad")
			}

			return roads[rnd]
		}
	}
	// Returning a loopback road because all of the destinations are busy by other monsters!
	return Road{
		destination: city,
	}
}

// DestroyCity destroys the city and all of the roads to/from it
func (world *World) DestroyCity(city *City) {
	world.lock.Lock()
	var index int
	for index = 0; index < len(world.cities) && world.cities[index] != city; index++ {
	}
	if index == len(world.cities) {
		log.Fatal("City not found! ", city.name)
		return
	}
	// Removing roads to this city:
	for _, road := range world.roads[city.name] {
		destRoads := world.roads[road.destination.name]
		for i, destRoad := range destRoads {
			if destRoad.destination == city {
				world.roads[road.destination.name] = append(destRoads[0:i], destRoads[i+1:]...)
			}
		}
	}
	// Removing all of roads from the City:
	delete(world.roads, city.name)
	// Removing City:
	world.cities = append(world.cities[0:index], world.cities[index+1:]...)
	world.lock.Unlock()
}

// DestroyMonster will remove the monster from all of cities in the world (useful for a very rare happenings)
func (world *World) DestroyMonster(monster *Monster, anotherMonsterID int) {
	world.lock.Lock()
	monster.isDestroyed = true
	for _, city := range world.cities {
		for index, monsterID := range city.monsters {
			if monsterID == monster.id || monsterID == anotherMonsterID {
				city.monsters = append(city.monsters[:index], city.monsters[index+1:]...)
			}
		}
	}
	world.lock.Unlock()
}
