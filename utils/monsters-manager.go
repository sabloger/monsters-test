package utils

import (
	"fmt"
	"sync"
)

// Monster struct keeps the monster's data
type Monster struct {
	id, Moves   int
	CurrentCity *City
	isDestroyed bool
}

var (
	monstersWG = sync.WaitGroup{}
)

// BuildMonsters will run monsters in the specified number
func BuildMonsters(no int) {
	for i := 0; i < no; i++ {
		monstersWG.Add(1)
		go (Monster{
			id:    i + 1,
			Moves: 0,
		}).RunMonster()
	}
	monstersWG.Wait()
}

// RunMonster will run the monster in the world!
func (monster Monster) RunMonster() {
	// Getting the starting city randomly
	city := WorldMap.GetRandomCity(monster.id)

	// Run until destroy or doing 10000 steps
	for monster.Moves = 0; monster.Moves < 10000 && !monster.isDestroyed; monster.Moves++ {
		if anotherMonster := city.CheckFight(monster.id); anotherMonster >= 0 {

			fmt.Printf("%s has been destroyed by monster %d and monster %d!\n", city.name, monster.id, anotherMonster)
			WorldMap.DestroyCity(city)
			WorldMap.DestroyMonster(&monster, anotherMonster)
			city.isDestroyed = true
			break
		} else if anotherMonster == -2 || !city.RemMonster(monster.id) {
			// The city is destroyed and i was the first monster on it! not the second one!
			WorldMap.DestroyMonster(&monster, -1)
			city.isDestroyed = true
			break
		}
		city = WorldMap.GetRandomRoad(city, monster.id).destination
	}
	// Telling WaitGroup that i am done!
	monstersWG.Done()
}
