package main

import (
	"fmt"
	"gitlab.com/sabloger/monsters-test/utils"
)

func main() {
	// Parsing command line arguments
	args := utils.ParseArgs()

	// Parsing world map file and build it in the memory as a Graph
	utils.ParseWorldMapFile(args.FileName)

	// Build and run the monsters to destroy the world!
	utils.BuildMonsters(args.N)

	fmt.Println("---------------- Finished: ------------------------->")

	// Print out whatever is left of the world in the same format as the input file
	utils.WorldMap.Print()
}
